import name from './variables';
import cat from './cat.jpg';
import meow from './meow.mp3';
import './style.scss';

const func = () => console.log('Hello there', name);

func();

const meowAudio = new Audio(meow);
const catImage = new Image();
catImage.src = cat;
catImage.addEventListener('click', () => meowAudio.play());
document.querySelector('body').appendChild(catImage);
